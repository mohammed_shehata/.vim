"================================== Vundle ==================================

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'terryma/vim-expand-region'
Plugin 'sjl/badwolf'
Plugin 'kien/ctrlp.vim'
Plugin 'romainl/Apprentice'
" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"==============================================================================







"Take a look to:
"https://github.com/sheerun/blog/blob/master/_posts/2014-03-21-how-to-boost-your-vim-productivity.markdown

colorscheme apprentice
" Make the gutters darker than the background.
let g:badwolf_darkgutter = 1

" Space is the leader key
let mapleader = "\<Space>"
"Enter visual line mode with <Space><Space>
nmap <Leader><Leader> V
"Copy & paste to system clipboard with <Space>p and <Space>y
vmap <Leader>y "+y
nmap <Leader>p "+p

"vim-expand-region
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink


"execute pathogen#infect()
syntax on
filetype plugin indent on

autocmd FileType ruby retab
autocmd FileType ruby setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
autocmd FileType haml setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
set number

"Type <Space>o to open a new file:
nnoremap <Leader>o :CtrlP<CR>

"Type <Space>w to save file (a lot faster than :w<Enter>):
nnoremap <Leader>w :w<CR>

set cursorline          " highlight current line
filetype indent on      " load filetype-specific indent files

" CtrlP settings
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 0
"let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'

set nowrap
